<?php
 include('dns.php');
?>
<!DOCTYPE html>
  <html>
    <head>
      <link rel="shortcut icon" href="/images/king-icon.png">

      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link rel="stylesheet" href="style.css">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Ferramenta de Email</title>
    </head>

    <body>
        <div class="titulo">
            <i id="mail-icon" class="large material-icons">mail</i>
        </div>
        <div class="form"  class="input-field col s12">
                <form method="POST" action="index.php">
                    <div class="insert-mail">
                    <p>Insira seu e-mail no campo abaixo: <input id="email" name="email" type="email" class="validate" id="conta" placeholder="Digite seu e-mail"></p>
                    <p id="butao">
                    <button id="submeter" type="submit" class="waves-effect waves-light btn" name="button">Enviar</button>

                  </div>
                    </p>
                </form>

                   <div><?php echo $h1 ?></div> 
                   <div class="prot-dom">
                    <h5 id="exibe"><?php echo $smtp;?></h5>
                    <h5 id="exibe"><?php echo $imap;?></h5>
                    <h5 id="exibe"><?php echo $pop;?></h5>
                    </div>
                    <br>
                    <h4><?php echo $h2 ?></h4>
                    <div class="prot-def">
                    <h5 id="exibe"><?php echo $kingSmtp;?></h5>
                    <h5 id="exibe"><?php echo $kingImap;?></h5>
                    <h5 id="exibe"><?php echo $kingPop;?></h5>
                    <p id="link"><?php echo $ssl; ?></p>
                   <!-- <p><? //php echo $retorno; ?></p> -->
                    </div>
              </div>
        </div>
         <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script src="jquery-3.4.1.min.js"></script>
    <!--  <script src="script.js"></script>-->
    </body>
  </html>
